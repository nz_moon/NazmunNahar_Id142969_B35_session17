<?php
class studentInfo
{
    public $student_id = "";
    public $student_name = "";
    public $student_CGPA = 0.00;

    public function set_id($id)
    {
        $this->student_id = $id;
        echo($this->student_id);
    }

}

$obj=new studentInfo;  //if we pass any parameter ,then we use parenthesis.otherwise it's necessary//
$obj->set_id("SEIP142969");
?>

<br>
<br>
<br>

<?php
class studentInformation
{
    public $student_id = "";
    public $student_name = "";
    public $student_CGPA = 0.00;

    public function set_id($id)
    {
        $this->student_id = $id;
    }

    public function set_std_name($name)
    {
        $this->student_name = $name;
    }

    public function set_std_cgpa($cgpa)
    {
        $this->student_CGPA = $cgpa;
    }

    public function get_std_id()
    {
        return $this->student_id;
    }

    public function get_Std_name()
    {
        return $this->student_name;
    }

    public function get_student_CGPA()
    {
        return $this->student_CGPA;
    }

    public function ShowDetails(){
        echo "ID: ".$this->student_id."<br>";
        echo "Name: ".$this->student_name."<br>";
        echo "CGPA: ".$this->student_CGPA."<br>";
    }

}

$obj=new studentInformation;  //if we pass any parameter ,then we use parenthesis.otherwise it's necessary//
$obj->set_id("SEIP142969");
$obj->set_std_name("Sadaf");
$obj->set_std_cgpa(3.36);

//echo $obj->get_std_id()."<br>";
//echo $obj->get_std_name()."<br>";
//echo $obj->get_student_CGPA()."<br>";

$obj->ShowDetails();
?>

